#include <stdio.h>
#include <pthread.h>
#include <socket.h>

int main(int argc, char const *argv[])
{
    unsigned short porta;

    if (argc != 2) {
		printf("Uso: %s <Porta>\n", argv[0]);
		return 1;
	}

	porta = atoi(argv[1]);
    int servidorSocket = cria_servidor(porta);
    
    pthread_t sockets_thread;
    pthread_create(&sockets_thread, NULL, &trata_conexao, &servidorSocket);

    while (1)
    {
        /* code */
    }

    close(servidorSocket);

    return 0;
}
