#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

void* trata_conexao(void *servidor) {
    int servidorSocket = (int *) servidor;
	char buffer[16];
	int tamanhoRecebido;
    int socketCliente;
    struct sockaddr_in clienteAddr;
    unsigned int clienteTamanho;

    while(1) {
		clienteTamanho = sizeof(clienteAddr);
		if((socketCliente = accept(servidorSocket, 
			                      (struct sockaddr *) &clienteAddr, 
			                      &clienteTamanho)) < 0)
			printf("Falha no Accept\n");
		
		printf("Conexão do Cliente %s\n", inet_ntoa(clienteAddr.sin_addr));

		if((tamanhoRecebido = recv(socketCliente, buffer, 16, 0)) < 0)
    		printf("Erro no recv()\n");

        while (tamanhoRecebido > 0) {
            if(send(socketCliente, buffer, tamanhoRecebido, 0) != tamanhoRecebido)
                printf("Erro no envio - send()\n");
            
            if((tamanhoRecebido = recv(socketCliente, buffer, 16, 0)) < 0)
                printf("Erro no recv()\n");
        }	
		
		close(socketCliente);
	}
}

int cria_servidor(unsigned short porta) {
	int servidorSocket;
	struct sockaddr_in servidorAddr;

	// Abrir Socket
	if((servidorSocket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
		printf("falha no socker do Servidor\n");

	// Montar a estrutura sockaddr_in
	memset(&servidorAddr, 0, sizeof(servidorAddr)); // Zerando a estrutura de dados
	servidorAddr.sin_family = AF_INET;
	servidorAddr.sin_addr.s_addr = inet_addr("localhost");
	servidorAddr.sin_port = htons(porta);

	// Bind
	if(bind(servidorSocket, (struct sockaddr *) &servidorAddr, sizeof(servidorAddr)) < 0)
		printf("Falha no Bind\n");

	// Listen
	if(listen(servidorSocket, 10) < 0)
		printf("Falha no Listen\n");
    
    return servidorSocket;
}